/* Semi-public definitions for GDI class */

#define SAGAGFX_LIBNAME "sagagfx.hidd"
#define CLID_Hidd_SAGAGfx "hidd.gfx.sagagfx"

struct SAGAGFXBase
{
    struct Library library;    /* Common library header */
};
